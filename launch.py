import shutil
from pathlib import Path
from dataclasses import asdict, replace
from typing import List, Optional
import logging

from arma3server import Env, Mod, local, SteamCMD, Workshop, Arma3ServerCMD


class Launcher:
    """Arma 3 server launcher."""

    def __init__(self, env: Env, logger: logging.Logger,
                 skip_install: Optional[bool] = None) -> None:
        self._env = env
        self._skip_install = skip_install
        self._logger = logger

        self._config_dir: Path = Path("")
        """Arma 3 server configuration directory."""

        self._config_path: Path = Path("")
        """Arma 3 server configuration file."""

        self._mods_dir: Path = Path("")
        """Arma 3 mods directory."""

        self._mods: List[Mod] = []
        """Arma 3 mods."""

        self._servermods: List[Mod] = []
        """Arma 3 server mods."""

        self._keys_dir: Path = Path("")
        """Arma 3 keys directory."""

    def __setup_keys(self) -> None:
        self._keys_dir = Path(self._env.arma_install_dir) / "keys"

        if not self._keys_dir.is_dir():
            self._logger.info("creating server keys folder")
            self._keys_dir.unlink(missing_ok=True)
            self._keys_dir.mkdir()

    def __install_arma_server(self) -> None:
        arma_app_id = "233780"

        self._logger.info("installing arma 3 server")
        SteamCMD(force_install_dir=self._env.arma_install_dir) \
            .login(user=self._env.steam_user, password=self._env.steam_password) \
            .app_update(
                id=arma_app_id,
                steam_branch=self._env.steam_branch,
                steam_branch_password=self._env.steam_branch_password) \
            .execute()

    def __render_config_file(self) -> None:
        from jinja2 import Template

        if self._env.arma_config is None:
            return

        self._logger.info("rendering server config file")

        template = Template(self._env.arma_config.read_text())

        self._config_dir = Path(self._env.arma_install_dir) / "configs"

        # create config directory if not exists
        self._config_dir.mkdir(parents=True, exist_ok=True)

        config_name = self._env.arma_config.name.removesuffix(
            "".join(self._env.arma_config.suffixes))
        self._config_path = self._config_dir / f"{config_name}.cfg"
        self._config_path.write_text(template.render(**asdict(self._env)))

        self._logger.info(self._config_path.read_text())

    def __setup_mpmissions(self) -> None:
        if self._env.mpmissions_dir is None:
            return

        self._logger.info("copying MP missions file")
        mpmissions_dir = Path(self._env.arma_install_dir) / "mpmissions"

        shutil.copytree(self._env.mpmissions_dir, mpmissions_dir, dirs_exist_ok=True)

        self._logger.info("done")

    def __normalize_mod(self, mod: Mod) -> Mod:
        self._logger.debug(f"configuring mod {mod.name}...")

        mod_name = mod.name.strip("\"'").lstrip("@").replace(" ", "_").lower()

        source_root = mod.dir
        target_root = Path(self._mods_dir).joinpath(Path(mod_name))

        path: Path
        for path in source_root.resolve().glob('**/*'):
            if path.is_file():  # create target symlink
                # convert to list so that we can change elements
                parts = list(path.parts)
                subparts = parts[parts.index(source_root.name)+1:]
                # lowercase every subdirectory
                subparts = map(str.lower, subparts)
                target_path = target_root.joinpath(Path(*subparts))
                if target_path.exists():
                    continue
                # ensure parent directory exists
                target_path.parent.mkdir(parents=True, exist_ok=True)
                target_path.symlink_to(path)

        # copy keys
        key: Path
        for key in source_root.rglob('**/*.bikey'):
            if not key.is_dir():
                shutil.copy2(key, self._keys_dir.as_posix())

        return replace(mod, name=mod_name,
                       dir=target_root.relative_to(self._env.arma_install_dir))

    def __setup_mods(self) -> None:
        _mods: List[Mod] = []
        _servermods: List[Mod] = []

        if self._env.mods_preset:
            self._logger.info("adding preset mods")
            _mods.extend(Workshop().preset(self._env.mods_preset,
                                           steam_user=self._env.steam_user,
                                           steam_password=self._env.steam_password))

        if (self._env.mods_local and self._env.mods_dir is not None and
                self._env.mods_dir.exists()):
            self._logger.info("adding local mods")
            _mods.extend(local(self._env.mods_dir))

        if self._env.servermods_dir is not None and self._env.servermods_dir.exists():
            self._logger.info("adding server mods")
            _servermods.extend(local(self._env.servermods_dir))

        # create mods directory if not exists
        self._mods_dir = Path(self._env.arma_install_dir) / "mods"
        self._mods_dir.mkdir(exist_ok=True)

        self._mods = [self.__normalize_mod(m) for m in _mods]
        self._servermods = [self.__normalize_mod(m) for m in _servermods]

    def run(self) -> None:
        if self._skip_install in [None, False] and not self._env.skip_install:
            self.__install_arma_server()
        self.__render_config_file()
        self.__setup_mpmissions()
        self.__setup_keys()
        self.__setup_mods()

        # run arma 3 server
        self._logger.info("running arma 3 dedicated server...")
        Arma3ServerCMD(arma_install_dir=self._env.arma_install_dir,
                       arma_binary=self._env.arma_binary) \
            .port(port=self._env.port) \
            .limit_fps(limit_fps=self._env.arma_limitfps) \
            .world(world=self._env.arma_world) \
            .params(params=self._env.arma_params) \
            .config(self._config_path) \
            .mods(self._mods) \
            .servermods(self._servermods) \
            .cdlc(arma_cdlc=self._env.arma_cdlc) \
            .profile(name=self._env.arma_profile,
                     profiles=self._config_dir / "profiles") \
            .execute()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description='Arma 3 Dedicated Server Launcher.')
    parser.add_argument("-p", "--port", dest="port", type=int,
                        default=None, help="Arma 3 server listen port")
    parser.add_argument("-c", "--config", dest="arma_config",
                        type=Path, default=None, help="Arma 3 server config file")
    parser.add_argument("-b", "--binary", dest="arma_binary",
                        type=Path, default=None, help="Arma 3 server binary file")
    parser.add_argument("-w", "--world", dest="arma_world",
                        type=str, default=None, help="Arma 3 world")
    parser.add_argument("--cdlc", dest="arma_cdlc", type=str,
                        default=None, help="Arma 3 cdlc")
    parser.add_argument("--params", dest="arma_params",
                        type=str, default=None, help="Arma 3 additional params")
    parser.add_argument("--mods-local", dest="mods_local",
                        action="store_true", default=None,
                        help="Arma 3 enable local mods")
    parser.add_argument("--mods-dir", dest="mods_dir", type=Path,
                        default=None, help="Arma 3 local mods directory")
    parser.add_argument("--servermods-dir", dest="servermods_dir",
                        type=Path, default=None,
                        help="Arma 3 local server mods directory")
    parser.add_argument("--mpmissions-dir", dest="mpmissions_dir", type=Path,
                        default=None, help="Arma 3 MP missions directory")
    parser.add_argument("--preset", dest="mods_preset",
                        type=Path, default=None, help="Arma 3 mods preset")
    parser.add_argument("--profile", dest="arma_profile",
                        type=Path, default=None, help="Arma 3 profile")
    parser.add_argument("--limitfps", dest="limit_fps",
                        type=int, default=None, help="limit fps")
    parser.add_argument("--skip-install", dest="skip_install", action="store_true",
                        default=None, help="skip Arma 3 server installation")
    args = parser.parse_args()

    arma_install_dir = Path.home() / "Steam/steamapps/arma3"

    env = Env(arma_install_dir=arma_install_dir,
              skip_install=args.skip_install,
              arma_config=args.arma_config,
              arma_binary=args.arma_binary,
              mods_local=args.mods_local,
              mods_dir=args.mods_dir,
              mods_preset=args.mods_preset,
              servermods_dir=args.servermods_dir,
              mpmissions_dir=args.mpmissions_dir,
              arma_world=args.arma_world,
              arma_limitfps=args.limit_fps,
              arma_profile=args.arma_profile,
              arma_params=args.arma_params,
              arma_cdlc=args.arma_cdlc,
              port=args.port)

    logger = logging.getLogger()

    ch = logging.StreamHandler()
    formatter = logging.Formatter(
        "[%(asctime)s][%(name)s][%(levelname)s] - %(message)s")
    ch.setFormatter(formatter)

    logger.addHandler(ch)
    log_level = "INFO"
    if env.log_level is not None:
        log_level = env.log_level
    logger.setLevel(getattr(logging, log_level, "INFO"))

    logger.info(env.tabulate())

    Launcher(env, logger=logger).run()
