FROM cm2network/steamcmd

LABEL maintainer="Matteo Arella - gitlab.com/matteoarella"

USER root

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    apt-get install -y python3 python3-pip \
    net-tools libsdl2-2.0-0 tree

RUN mkdir -p /home/steam/Steam/steamapps/arma3
RUN mkdir -p /home/steam/Steam/steamapps/workshop

RUN chown -R steam:steam /home/steam/Steam/steamapps/arma3
RUN chown -R steam:steam /home/steam/Steam/steamapps/workshop

USER steam

ENV ARMA_BINARY=arma3server_x64
ENV PORT=2302
ENV SKIP_INSTALL=false
ENV ARMA_PROFILE=main
ENV ARMA_WORLD=empty
ENV ARMA_LIMITFPS=1000
ENV HEADLESS_CLIENTS=0
ENV HEADLESS_CLIENTS_PROFILE="\$profile-hc-\$i"
ENV STEAM_BRANCH=public
ENV STEAM_BRANCH_PASSWORD=
ENV PATH="${PATH}:${HOMEDIR}/steamcmd:${HOMEDIR}/.local/bin"

RUN python3 -m pip install --user --upgrade pip && \
    python3 -m pip install --user pipenv

STOPSIGNAL SIGINT

COPY --chown=steam:steam Pipfile* .

RUN pipenv sync

COPY --chown=steam:steam arma3server arma3server
COPY --chown=steam:steam launch.py launch.py

ENTRYPOINT ["pipenv", "run", "python", "launch.py"]
