#!/usr/bin/env sh
set -e

BASEDIR=$(dirname $(realpath "$0"))
ME=$(basename "$0")

usage() {
  cat <<EOF
Usage: $ME COMMAND

Commands:
    init                Init docker swarm cluster
    deploy              Deploy services to swarm cluster
    services            List services
    ps                  List the tasks in the stack
    inspect             Inspect services
    logs [args]         Fetch the logs of a service or task
    stop                Remove services
    destroy             Destroy docker swarm cluster
    -h, --help          show this help and exit
EOF
}

STACK_NAME=$(echo $(basename "$BASEDIR") | tr '[:upper:]' '[:lower:]')

init() {
    # initialize swarm manager
    docker swarm init --advertise-addr 127.0.0.1
}

deploy() {
    (
        [ -f "$BASEDIR/.env" ] && set -o allexport; . "$BASEDIR/.env"; set +o allexport
        [ -f "$BASEDIR/env/.prod.env" ] && set -o allexport; . "$BASEDIR/env/.prod.env"; set +o allexport
        docker stack deploy -c production.yml $STACK_NAME
    )
}

list() {
    docker stack ps $STACK_NAME
}

services() {
    docker service ls
}

inspect() {
    docker service inspect --pretty "${STACK_NAME}_arma3"
}

logs() {
    docker service logs "${STACK_NAME}_arma3" $1
}

stop() {
    docker stack rm $STACK_NAME
}

destroy() {
    docker swarm leave --force
}

case "$1" in
    init)
        init
        ;;
    deploy)
        deploy && logs "$2"
        ;;
    services)
        services
        ;;
    ps)
        list
        ;;
    inspect)
        inspect
        ;;
    logs)
        logs "$2"
        ;;
    stop)
        stop
        ;;
    destroy)
        destroy
        ;;
    -h|--help)
        usage
        exit 0
        ;;
    *)
        usage
        exit 1
        ;;
esac
