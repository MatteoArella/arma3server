Il file `docker-compose.yml` contiene la lista delle mod da caricare.

```yaml
    arma3:
        ...
        volumes:
        - ...
        - './mods/@CBA_A3:/arma3/mods/@CBA_A3'
        - ...
```

La direttiva ```./mods/@CBA_A3:/arma3/mods/@CBA_A3``` indica `path_locale:path_dentro_al_container`, quindi `./mods/@CBA_A3` e' la cartella locale del pc
che contiene la mod.


Starta il server con
```bash
sudo docker compose up
```

Stoppalo con
```bash
sudo docker compose down
```

riconfigurazione docker
docker volume ls
docker volume rm <nome>

```
DST Port	Protocol	Destination	Comment
2344	TCP + UDP	81.0.236.111	BattlEye - arma31.battleye.com
2345	TCP	81.0.236.111	BattlEye - arma31.battleye.com
2302-2306	UDP	any	Arma Server to Client Traffic
2303	UDP	any	Arma Server STEAM query port
2304	UDP	any	Arma Server to STEAM master traffic
```