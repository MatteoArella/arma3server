from pathlib import Path
import os
import pytest
from unittest import mock

from arma3server import Env

class TestEnv:
    MOCK_ENVIRON = {
        "ARMA_BINARY": "arma3server_x64",
        "ARMA_PARAMS": "",
        "ARMA_PROFILE": "main",
        "ARMA_WORLD": "empty",
        "ARMA_LIMITFPS": "1000",
        "ARMA_CDLC": "",
        "HEADLESS_CLIENTS": "0",
        "HEADLESS_CLIENTS_PROFILE": "\\$profile-hc-\\$i",
        "PORT": "2302",
        "STEAM_USER": "steam-id",
        "STEAM_PASSWORD": "steam-password",
        "STEAM_BRANCH": "public",
        "STEAM_BRANCH_PASSWORD": "",
        "MODS_LOCAL": "true",
        "MODS_PRESET": "",
        "SKIP_INSTALL": "false",
    }

    @pytest.fixture(autouse=True)
    def mock_settings_environ(self):
        with mock.patch.dict(os.environ, TestEnv.MOCK_ENVIRON, clear=True):
            yield

    def test_env_configured_from_os_environ(self):
        e = Env(arma_install_dir=Path())
        assert e.arma_binary == Path(TestEnv.MOCK_ENVIRON["ARMA_BINARY"])
        assert e.arma_params == None
        assert e.arma_profile == Path(TestEnv.MOCK_ENVIRON["ARMA_PROFILE"])
        assert e.arma_world == TestEnv.MOCK_ENVIRON["ARMA_WORLD"]
        assert e.arma_limitfps == int(TestEnv.MOCK_ENVIRON["ARMA_LIMITFPS"])
        assert e.port == int(TestEnv.MOCK_ENVIRON["PORT"])
        assert e.steam_user == TestEnv.MOCK_ENVIRON["STEAM_USER"]
        assert e.steam_password == TestEnv.MOCK_ENVIRON["STEAM_PASSWORD"]

    def test_override_env_default_value(self):
        arma_binary=Path("arma_binary")
        e = Env(arma_install_dir=Path(), arma_binary=arma_binary)
        assert e.arma_binary == arma_binary
