from __future__ import annotations
from dataclasses import dataclass
from pathlib import Path
from typing import List


@dataclass
class Mod:
    """Arma 3 Mod."""

    name: str
    """The name of the mod."""

    dir: Path
    """The source path of the mod."""


def local(d: Path) -> List[Mod]:
    mods: List[Mod] = []

    # Find mod folders
    for m in d.iterdir():
        if m.is_dir():
            mods.append(Mod(name=m.name, dir=m))

    return mods
