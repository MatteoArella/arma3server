from dataclasses import dataclass
from pathlib import Path
from typing import List, Optional
import requests
from urllib.parse import urlparse, parse_qs
from bs4 import BeautifulSoup
from arma3server.mods import Mod

import arma3server.steamcmd as steamcmd


@dataclass
class Item(Mod):
    """Arma 3 mod from Steam workshop."""

    id: str
    """The id of the workshop item."""


class Workshop:
    ARMA_WORKSHOP = "107410"
    WORKSHOP = f"steamapps/workshop/content/{ARMA_WORKSHOP}"

    def item(self, id: str, name: str, steam_user: Optional[str] = None,
             steam_password: Optional[str] = None) -> Item:
        steamcmd.SteamCMD() \
            .login(user=steam_user, password=steam_password) \
            .workshop(workshop=Workshop.ARMA_WORKSHOP, item=id) \
            .execute()

        moddir = Path.home() / Path("Steam", Workshop.WORKSHOP, id)

        return Item(id=id, name=name, dir=moddir)

    def preset(self, preset_file: str, steam_user: Optional[str] = None,
               steam_password: Optional[str] = None) -> List[Item]:
        USER_AGENT = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) "
                      "AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/35.0.1916.47 Safari/537.36")
        mods: List[Item] = []

        preset_url = urlparse(preset_file)
        if preset_url.scheme.startswith("http"):
            response = requests.get(preset_file, headers={"User-Agent": USER_AGENT})
            if not response.ok:
                return mods
            html = response.text
        else:
            if not Path(preset_file).exists():
                return mods
            html = Path(preset_file).read_text()

        soup = BeautifulSoup(html, "html.parser")
        mod_container: BeautifulSoup
        for mod_container in soup.find_all(attrs={"data-type": "ModContainer"}):
            display_name = mod_container.find(attrs={"data-type": "DisplayName"})
            mod_name = ""
            if display_name is not None:
                mod_name = display_name.get_text()
            mod_url = mod_container.find("a", href=True)
            mod_id = ""
            if mod_url is not None:
                mod_id = parse_qs(urlparse(mod_url.get_text()).query)["id"][0]

            # download workshop item
            mods.append(self.item(mod_id, mod_name, steam_user=steam_user,
                                  steam_password=steam_password))

        return mods
