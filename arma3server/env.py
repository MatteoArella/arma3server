from dataclasses import dataclass, field, fields
import json
import os
from pathlib import Path
from urllib.parse import urlparse
from typing import Any, Mapping, Optional, Union, get_origin, get_args
from tabulate import tabulate


def conf(exclude: bool = False) -> Mapping[Any, Any]:
    return {"exclude": exclude}


@dataclass
class Env:
    arma_install_dir: Path
    """Arma 3 server installation directory."""

    log_level: Optional[str] = None
    """Logging level."""

    port: Optional[int] = None
    """Arma 3 server port."""

    skip_install: Optional[bool] = None
    """Skip arma 3 server installation."""

    arma_config: Optional[Path] = None
    """Arma 3 server config file."""

    arma_binary: Optional[Path] = None
    arma_limitfps: Optional[int] = None
    arma_world: Optional[str] = None
    arma_params: Optional[str] = None
    arma_cdlc: Optional[str] = None
    arma_profile: Optional[Path] = None
    headless_clients: Optional[str] = None
    headless_clients_profile: Optional[str] = None

    mods_preset: Optional[str] = None
    """Arma 3 mods preset to use."""

    mods_local: Optional[bool] = None
    """Load Arma 3 local mods."""

    mods_dir: Optional[Path] = None
    """Arma 3 local mods directory."""

    mpmissions_dir: Optional[Path] = None
    """Arma 3 MP directory."""

    servermods_dir: Optional[Path] = None
    """Arma 3 local server mods directory."""

    steam_user: Optional[str] = None
    """Steam account user id."""

    steam_password: Optional[str] = field(default=None, repr=False,
                                          metadata=conf(exclude=True))
    """Steam account password."""

    steam_branch: Optional[str] = None
    steam_branch_password: Optional[str] = field(default=None, repr=False,
                                                 metadata=conf(exclude=True))

    # server configuration
    server_hostname: Optional[str] = None
    """The name of the server that shall be displayed in the public server list."""

    server_password: Optional[str] = field(default=None, repr=False,
                                           metadata=conf(exclude=True))
    """The password for joining the server."""

    server_password_admin: Optional[str] = field(default=None, repr=False,
                                                 metadata=conf(exclude=True))
    """The admin password for the server."""

    def __is_optional_field(self, field: Any) -> bool:
        return get_origin(field) is Union and \
           type(None) in get_args(field)

    def __resolve(self, value: str) -> str:
        """ Resolve secret value """
        url = urlparse(value)
        if url.scheme == "secret":
            secret_url = urlparse(url.path)
            if secret_url.scheme == "file":
                return Path(secret_url.path).read_text()
        return value

    def __convert(self, field: Any, value: str) -> Any:
        types = get_args(field)
        if value is None:
            return None
        if Path in types:
            return Path(value)
        elif str in types:
            return self.__resolve(value)
        try:
            return json.loads(str(value))
        except Exception:
            return None

    def __env_get_property(self, key: str) -> Optional[str]:
        return os.environ[key] if key in os.environ and os.environ[key] else None

    def __set_property(self, key: str, val: Any) -> None:
        if getattr(self, key) is None:
            setattr(self, key, val)

    def __post_init__(self) -> None:
        # extract class fields mapping to uppercase
        properties = {f.name: f.type for f in fields(self)
                      if self.__is_optional_field(f.type)}

        for attr_name, attr_type in properties.items():
            value = self.__env_get_property(attr_name.upper())
            if value is None:
                continue
            self.__set_property(attr_name, self.__convert(attr_type, value))

    def tabulate(self) -> str:
        exported = [f.name for f in fields(self)
                    if not f.metadata.get("exclude", False)]
        data = [[attr, "-" if getattr(self, attr) is None else
                 getattr(self, attr)] for attr in exported]
        return tabulate(data, headers=["property", "value"],
                        tablefmt="outline", colalign=("right", "left"))
