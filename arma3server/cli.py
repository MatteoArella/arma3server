from __future__ import annotations
from typing import IO, Any, List, TypeVar, Generic, Union
import subprocess
import select
import logging

_T = TypeVar("_T")


class _BaseArgument(Generic[_T]):
    def __init__(self, argument: _T, sensitive: bool = False) -> None:
        self.argument = argument
        self.sensitive = sensitive

    def __str__(self) -> str:
        return self.argument.__str__() if not self.sensitive else "<redacted>"

    def __repr__(self) -> str:
        return self.__str__()


class StringArgument(_BaseArgument[str]):
    pass


class CMD:
    def __init__(self, logger: logging.Logger, stdout_log_level: int = logging.DEBUG,
                 stderr_log_level: int = logging.ERROR) -> None:
        self._cmd: list[Union[str, StringArgument]] = []
        self._logger = logger
        self._stdout_log_level = stdout_log_level
        self._stderr_log_level = stderr_log_level

    def add(self, *args: Union[str, StringArgument]) -> CMD:
        args_len = len(args)

        if args_len == 1:
            self._cmd.append(args[0])
        elif args_len > 1:
            self._cmd.extend(args)

        return self

    # https://gist.github.com/bgreenlee/1402841
    def __call(self, args: Any, logger: logging.Logger, stdout_log_level: int,
               stderr_log_level: int, **kwargs: Any) -> int:
        """
        Variant of subprocess.call that accepts a logger instead of stdout/stderr,
        and logs stdout messages via logger.debug and stderr messages via
        logger.error.
        """
        child = subprocess.Popen(args=args, stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE, text=True, **kwargs)
        if child.stdout is None or child.stderr is None:
            raise IOError()

        log_level = {child.stdout: stdout_log_level,
                     child.stderr: stderr_log_level}

        def check_io():
            timeout = 1e-6
            ready_to_read: List[IO[str]] = select.select([child.stdout, child.stderr],
                                                         [], [], timeout)[0]
            try:
                for io in ready_to_read:
                    line = io.readline()
                    logger.log(log_level[io], line[:-1])
            except UnicodeDecodeError:
                pass

        # keep checking stdout/stderr until the child exits
        while child.poll() is None:
            check_io()

        check_io()  # check again to catch anything after the process exits

        return child.wait()

    def execute(self, **kwargs: Any) -> int:
        self._logger.info(" ".join(map(str, self._cmd)))

        args = [arg.argument if isinstance(arg, StringArgument) else
                arg for arg in self._cmd]

        return self.__call(args,
                           logger=self._logger,
                           stdout_log_level=self._stdout_log_level,
                           stderr_log_level=self._stderr_log_level, **kwargs)
