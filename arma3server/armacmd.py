from __future__ import annotations
from typing import Any, List, Optional
from pathlib import Path
import logging

from arma3server.cli import CMD
from arma3server.mods import Mod

_logger = logging.getLogger(__name__)


class Arma3ServerCMD(CMD):
    def __init__(self, arma_install_dir: Path,
                 arma_binary: Optional[Path] = None) -> None:
        super().__init__(logger=_logger, stdout_log_level=logging.INFO,
                         stderr_log_level=logging.INFO)
        self._arma_install_dir = arma_install_dir
        arma_binary = arma_binary if arma_binary is not None else Path("arma3server")
        super().add(f"./{arma_binary.name}")

    def port(self, port: Optional[int] = None) -> Arma3ServerCMD:
        if port is not None:
            super().add(f"-port={port}")
        return self

    def world(self, world: Optional[str] = None) -> Arma3ServerCMD:
        if world is not None:
            super().add(f"-world={world}")
        return self

    def limit_fps(self, limit_fps: Optional[int] = None) -> Arma3ServerCMD:
        if limit_fps is not None:
            super().add(f"-limitFPS={limit_fps}")
        return self

    def mods(self, mods: List[Mod]) -> Arma3ServerCMD:
        if len(mods) > 0:
            mods_param = ';'.join(map(lambda m: m.dir.as_posix(), mods))
            super().add(f"-mod=\"{mods_param}\"")
        return self

    def servermods(self, servermods: List[Mod]) -> Arma3ServerCMD:
        if len(servermods) > 0:
            mods_param = ';'.join(map(lambda m: m.dir.as_posix(), servermods))
            super().add(f"-serverMod=\"{mods_param}\"")
        return self

    def cdlc(self, arma_cdlc: Optional[str] = None) -> Arma3ServerCMD:
        if arma_cdlc is not None:
            super().add(f"-mod=\"{arma_cdlc}\"")
        return self

    def profile(self, name: Optional[Path] = None,
                profiles: Optional[Path] = None) -> Arma3ServerCMD:
        if name is not None:
            super().add(f"-name=\"{name.as_posix()}\"")
        if profiles is not None:
            super().add(f"-profiles=\"{profiles.as_posix()}\"")
        return self

    def params(self, params: Optional[str] = None) -> Arma3ServerCMD:
        if params is not None:
            super().add(params)
        return self

    def config(self, config: Path) -> Arma3ServerCMD:
        super().add(f"-config=\"{config.as_posix()}\"")
        return self

    def execute(self, **kwargs: Any) -> int:
        return super().execute(cwd=self._arma_install_dir.as_posix(), **kwargs)
