from __future__ import annotations
from pathlib import Path
from typing import Any, Optional
import logging
from arma3server.cli import CMD, StringArgument

_logger = logging.getLogger(__name__)


class SteamCMD(CMD):
    def __init__(self, force_install_dir: Optional[Path] = None) -> None:
        super().__init__(logger=_logger, stdout_log_level=logging.INFO)
        super().add("steamcmd.sh")
        if force_install_dir is not None:
            super().add("+force_install_dir", force_install_dir.as_posix())

    def login(self, user: Optional[str] = None,
              password: Optional[str] = None) -> SteamCMD:
        user = user if user is not None else "anonymous"

        super().add("+login", user)
        if password is not None:
            super().add(StringArgument(password, sensitive=True))

        return self

    def app_update(self, id: str, steam_branch: Optional[str] = None,
                   steam_branch_password: Optional[str] = None,
                   validate: bool = True) -> SteamCMD:
        super().add("+app_update", id)

        if steam_branch is not None:
            super().add("-beta", steam_branch)
        if steam_branch_password is not None:
            super().add("-betapassword",
                        StringArgument(steam_branch_password, sensitive=True))

        if validate:
            super().add("validate")

        return self

    def workshop(self, workshop: str, item: str) -> SteamCMD:
        super().add("+workshop_download_item", workshop, item)
        return self

    def execute(self, **kwargs: Any) -> int:
        super().add("+quit")
        retcode = super().execute(**kwargs)
        if retcode != 0:
            _logger.error((Path.home() / "Steam/logs/stderr.txt").read_text())
        return retcode
